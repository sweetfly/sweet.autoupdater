﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AutoUpdater.Configs;
using AutoUpdater.Contracts;
using AutoUpdater.Implementations;
using AutoUpdater.Utilities;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace AutoUpdater
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //自动升级
            IAutoUpdater updater = new SweetAutoUpdater();

            try
            {
                updater.Update();
            }
            catch (System.Exception ex)
            {
                updater.RollBack();
                MessageBox.Show(ex.Message);
            }

        }
    }
}
