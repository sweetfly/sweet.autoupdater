﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

namespace AutoUpdater.Contracts
{
    public interface IAutoUpdater
    {
        /// <summary>
        /// 执行升级
        /// </summary>
        bool Update();

        /// <summary>
        /// 回滚
        /// </summary>
        bool RollBack();
    }
}
