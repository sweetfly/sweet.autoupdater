/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.IO;
using AutoUpdater.Configs;
using AutoUpdater.Models;
using AutoUpdater.Utilities;
using System.Linq;

namespace AutoUpdater.Forms
{
    public partial class DownloadProgress : Form
    {
        #region The private fields
        private readonly List<DownloadFileInfo> _downloadFileList = null;
        private readonly List<DownloadFileInfo> _allFileList = null;

        private bool _isFinished = false;
        private ManualResetEvent _evtDownload = null;
        private ManualResetEvent _evtPerDonwload = null;

        long _total = 0;
        long _nDownloadedTotal = 0;
        #endregion

        #region The constructor of DownloadProgress
        public DownloadProgress(List<DownloadFileInfo> downloadFileListTemp)
        {
            InitializeComponent();

            this._downloadFileList = downloadFileListTemp;
            _allFileList = new List<DownloadFileInfo>();
            foreach (DownloadFileInfo file in downloadFileListTemp)
            {
                _allFileList.Add(file);
            }
        }
        #endregion

        #region The method and event
        private void OnFormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_isFinished && DialogResult.No == MessageBox.Show(ConstFile.CANCELORNOT, ConstFile.MESSAGETITLE, MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                e.Cancel = true;
                return;
            }
            else
            {
                //if (_clientDownload != null)
                //    _clientDownload.CancelAsync();

                _evtDownload.Set();
                _evtPerDonwload.Set();
            }
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            _evtDownload = new ManualResetEvent(true);
            _evtDownload.Reset();
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.ProcDownload));
        }

        private void ProcDownload(object o)
        {
            string tempFolderPath = Path.Combine(CommonUnitity.SystemBinUrl, ConstFile.TEMPFOLDERNAME);
            if (!Directory.Exists(tempFolderPath))
            {
                Directory.CreateDirectory(tempFolderPath);
            }

            _evtPerDonwload = new ManualResetEvent(false);

            //计算需要下载的总大小（字节）
            _total = _downloadFileList.Sum(x => x.Size);

            try
            {
                while (!_evtDownload.WaitOne(0, false))
                {
                    if (this._downloadFileList.Count == 0)
                        break;

                    DownLoadFile(tempFolderPath);
                }

            }
            catch (Exception)
            {
                ShowErrorAndRestartApplication();
                //throw;
            }

            //When the files have not downloaded,return.
            if (_downloadFileList.Count > 0)
            {
                return;
            }

            //Test network and deal with errors if there have 
            DealWithDownloadErrors();

            //Debug.WriteLine("All Downloaded");
            foreach (DownloadFileInfo file in this._allFileList)
            {
                string tempUrlPath = CommonUnitity.GetFolderUrl(file);
                string oldPath = string.Empty;
                string newPath = string.Empty;
                try
                {
                    if (!string.IsNullOrEmpty(tempUrlPath))
                    {
                        oldPath = Path.Combine(CommonUnitity.SystemBinUrl + tempUrlPath.Substring(1), file.FileName);
                        newPath = Path.Combine(CommonUnitity.SystemBinUrl + ConstFile.TEMPFOLDERNAME + tempUrlPath, file.FileName);
                    }
                    else
                    {
                        oldPath = Path.Combine(CommonUnitity.SystemBinUrl, file.FileName);
                        newPath = Path.Combine(CommonUnitity.SystemBinUrl + ConstFile.TEMPFOLDERNAME, file.FileName);
                    }

                    //just deal with the problem which the files EndsWith xml can not download
                    System.IO.FileInfo f = new FileInfo(newPath);
                    if (!file.Size.ToString().Equals(f.Length.ToString()) && !file.FileName.ToString().EndsWith(".xml"))
                    {
                        ShowErrorAndRestartApplication();
                    }


                    //Added for dealing with the config file download errors
                    string newfilepath = string.Empty;
                    if (newPath.Substring(newPath.LastIndexOf(".") + 1).Equals(ConstFile.CONFIGFILEKEY))
                    {
                        if (System.IO.File.Exists(newPath))
                        {
                            if (newPath.EndsWith("_"))
                            {
                                newfilepath = newPath;
                                newPath = newPath.Substring(0, newPath.Length - 1);
                                oldPath = oldPath.Substring(0, oldPath.Length - 1);
                            }
                            File.Move(newfilepath, newPath);
                        }
                    }
                    //End added

                    if (File.Exists(oldPath))
                    {
                        MoveFolderToOld(oldPath, newPath);
                    }
                    else
                    {
                        //Edit for config_ file
                        if (!string.IsNullOrEmpty(tempUrlPath))
                        {
                            if (!Directory.Exists(CommonUnitity.SystemBinUrl + tempUrlPath.Substring(1)))
                            {
                                Directory.CreateDirectory(CommonUnitity.SystemBinUrl + tempUrlPath.Substring(1));


                                MoveFolderToOld(oldPath, newPath);
                            }
                            else
                            {
                                MoveFolderToOld(oldPath, newPath);
                            }
                        }
                        else
                        {
                            MoveFolderToOld(oldPath, newPath);
                        }

                    }
                }
                catch (Exception exp)
                {
                    //log the error message,you can use the application's log code
                }

            }

            //After dealed with all files, clear the data
            this._allFileList.Clear();

            //防止异步调用时退出 By Sweet 
            //while (this._downloadFileList.Count > 0)
            //{
            //    Thread.Sleep(500);
            //}

            if (this._downloadFileList.Count == 0)
                Exit(true);
            else
                Exit(false);

            _evtDownload.Set();
        }

        private string DownLoadFile(string tempFolderPath)
        {
            //读取一个文件信息
            DownloadFileInfo file = this._downloadFileList.FirstOrDefault();
            if (file == null) { return null; }

            //设置当前下载的文件名
            this.ShowCurrentDownloadFileName(file.FileName);

            //Download
            using (var _clientDownload = new WebClient())
            {
                //Added the function to support proxy
                //_clientDownload.Proxy = System.Net.WebProxy.GetDefaultProxy();
                _clientDownload.Proxy = WebRequest.GetSystemWebProxy();
                _clientDownload.Proxy.Credentials = CredentialCache.DefaultCredentials;
                _clientDownload.Credentials = System.Net.CredentialCache.DefaultCredentials;
                //End added

                #region 下载时，绑定修改界面的事件
                _clientDownload.DownloadProgressChanged += (object sender, DownloadProgressChangedEventArgs e) =>
                  {
                      try
                      {
                          this.SetProcessBar(e.ProgressPercentage, (int)((_nDownloadedTotal + e.BytesReceived) * 100 / _total));
                      }
                      catch
                      {
                          //log the error message,you can use the application's log code
                      }

                  };

                _clientDownload.DownloadFileCompleted += (object sender, AsyncCompletedEventArgs e) =>
                {
                    try
                    {
                        // DealWithDownloadErrors();
                        var dfile = e.UserState as DownloadFileInfo;
                        if (dfile == null) { return; }

                        _nDownloadedTotal += dfile.Size;
                        this.SetProcessBar(0, (int)(_nDownloadedTotal * 100 / _total));
                        _evtPerDonwload.Set();
                    }
                    catch (Exception)
                    {
                        //log the error message,you can use the application's log code
                    }

                };
                #endregion

                _evtPerDonwload.Reset();

                //Download the folder file
                string tempFolderPath1 = CommonUnitity.GetFolderUrl(file);
                if (!string.IsNullOrEmpty(tempFolderPath1))
                {
                    tempFolderPath = Path.Combine(CommonUnitity.SystemBinUrl, ConstFile.TEMPFOLDERNAME);
                    tempFolderPath += tempFolderPath1;
                }
                else
                {
                    tempFolderPath = Path.Combine(CommonUnitity.SystemBinUrl, ConstFile.TEMPFOLDERNAME);
                }

                _clientDownload.DownloadFileAsync(new Uri(file.DownloadUrl), Path.Combine(tempFolderPath, file.Path), file);

                //Wait for the download complete
                _evtPerDonwload.WaitOne();
            }

            //Remove the downloaded files
            this._downloadFileList.Remove(file);
            return tempFolderPath;
        }

        //To delete or move to old files
        void MoveFolderToOld(string oldPath, string newPath)
        {
            if (File.Exists(oldPath + ".old"))
                File.Delete(oldPath + ".old");

            if (File.Exists(oldPath))
                File.Move(oldPath, oldPath + ".old");



            File.Move(newPath, oldPath);
            //File.Delete(oldPath + ".old");
        }



        delegate void ExitCallBack(bool success);
        private void Exit(bool success)
        {
            if (this.InvokeRequired)
            {
                ExitCallBack cb = new ExitCallBack(Exit);
                this.Invoke(cb, new object[] { success });
            }
            else
            {
                this._isFinished = success;
                this.DialogResult = success ? DialogResult.OK : DialogResult.Cancel;
                this.Close();
            }
        }

        private void OnCancel(object sender, EventArgs e)
        {
            //bCancel = true;
            //evtDownload.Set();
            //evtPerDonwload.Set();
            ShowErrorAndRestartApplication();
        }

        //疑似无用的方法
        private void DealWithDownloadErrors()
        {
            try
            {
                //Test Network is OK or not.
                Config config = Config.GetInstance();

                using (var client = new WebClient())
                {
                    //下载升级配置文件
                    client.DownloadString(config.ServerUrl);
                }

            }
            catch (Exception)
            {
                //log the error message,you can use the application's log code
                ShowErrorAndRestartApplication();
            }
        }

        private void ShowErrorAndRestartApplication()
        {
            MessageBox.Show(ConstFile.NOTNETWORK, ConstFile.MESSAGETITLE, MessageBoxButtons.OK, MessageBoxIcon.Information);
            CommonUnitity.RestartApplication();
        }


        #endregion

        #region 修改界面

        /// <summary>
        /// 设置当前下载的文件名
        /// </summary>
        /// <param name="name"></param>
        private void ShowCurrentDownloadFileName(string name)
        {
            if (this.labelCurrentItem.InvokeRequired)
            {
                var cb = new Action<string>(ShowCurrentDownloadFileName);
                this.Invoke(cb, new object[] { name });
            }
            else
            {
                this.labelCurrentItem.Text = name;
            }
        }

        //delegate void SetProcessBarCallBack(int current, int total);
        /// <summary>
        /// 设置当前下载进度
        /// </summary>
        /// <param name="current"></param>
        /// <param name="total"></param>
        private void SetProcessBar(int current, int total)
        {
            if (this.progressBarCurrent.InvokeRequired)
            {
                var cb = new Action<int, int>(SetProcessBar);
                //SetProcessBarCallBack cb = new SetProcessBarCallBack(SetProcessBar);
                this.Invoke(cb, new object[] { current, total });
            }
            else
            {
                this.progressBarCurrent.Value = current;
                this.progressBarTotal.Value = total;
            }
        }

        #endregion
    }
}