﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AutoUpdater.Models;
using AutoUpdater.Utilities;
using System.IO;
using System.Threading;

namespace AutoUpdater.Forms
{
    public partial class SweetDownloadProgress : Form
    {
        #region 私有成员
        readonly Queue<DownloadFileInfo> _downloadQueue;
        private readonly string _baseDirectory = Environment.CurrentDirectory;

        readonly long _total = 0;
        long _nDownloadedTotal = 0;
        #endregion

        #region 构造函数

        public SweetDownloadProgress()
        {
            InitializeComponent();
        }

        public SweetDownloadProgress(List<DownloadFileInfo> downloadFileList)
            : this()
        {
            if (downloadFileList == null)
            {
                return;
            }
            _downloadQueue = new Queue<DownloadFileInfo>(downloadFileList);

            _total = downloadFileList.Sum(x => x.Size);
            _nDownloadedTotal = 0;
        }

        #endregion

        #region 私有方法
        void Success()
        {
            if (this.InvokeRequired)
            {
                var action = new Action(Success);
                this.Invoke(action);
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        void Failed()
        {
            if (this.InvokeRequired)
            {
                var action = new Action(Failed);
                this.Invoke(action);
            }
            else
            {
                DialogResult = DialogResult.Cancel;
                Close();
            }
        }

        //启动下载文件
        bool StartDownload()
        {
            if (_downloadQueue == null || _downloadQueue.Count <= 0)
            {
                //全部下载已经完成
                Success();
                return false;
            }

            //取得一个需要下载的文件
            var downloadItem = _downloadQueue.Dequeue();

            //界面显示
            ModifyDownloadingFileName(downloadItem.FileName, downloadItem.DownloadUrl);

            //备份原文件


            //下载新文件
            string newFileFullName = Path.Combine(_baseDirectory, downloadItem.Path);
            string filePath = Path.GetDirectoryName(newFileFullName);
            if (filePath != null && false == Directory.Exists(filePath)) { Directory.CreateDirectory(filePath); }

            HttpUtil.DownloadFileAsync(new Uri(downloadItem.DownloadUrl), newFileFullName, downloadItem,
                                            (o, args) =>
                                            {
                                                //当前文件下载进度
                                                ModifyCurrentProgressText(args.BytesReceived, args.TotalBytesToReceive);

                                                //所有下载进度
                                                ModifyTotalProgressText(args.BytesReceived);
                                            },
                                           (o, args) =>
                                           {
                                               var downloadFileInfo = args.UserState as DownloadFileInfo;
                                               if (downloadFileInfo != null) { _nDownloadedTotal += downloadFileInfo.Size; }

                                               //下载下一个文件
                                               StartDownload();
                                           });
            return true;

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            var thread = new Thread((o) => { StartDownload(); });
            thread.IsBackground = true;
            thread.Start();
        }

        #endregion

        #region 修改界面

        void ModifyDownloadingFileName(string fileName, string url)
        {
            if (lb_fileName.InvokeRequired || lb_url.InvokeRequired)
            {
                var action = new Action<string, string>(ModifyDownloadingFileName);
                this.Invoke(action, fileName, url);
            }
            else
            {
                lb_fileName.Text = fileName;
                lb_url.Text = url;
            }
        }

        void ModifyCurrentProgressText(long bytesReceived, long totalBytesToReceive)
        {
            if (progressBarCurrent.InvokeRequired)
            {
                var action = new Action<long, long>(ModifyCurrentProgressText);
                this.Invoke(action, new object[] { bytesReceived, totalBytesToReceive });
            }
            else
            {
                this.progressBarCurrent.Value = Convert.ToInt32(100 * bytesReceived / totalBytesToReceive);
                labelCurrent.Text = string.Format("{0} / {1}", bytesReceived, totalBytesToReceive);
            }
        }

        void ModifyTotalProgressText(long bytesReceived)
        {

            if (progressBarCurrent.InvokeRequired)
            {
                var action = new Action<long>(ModifyTotalProgressText);
                this.Invoke(action, bytesReceived);
            }
            else
            {
                long currentBytes = Math.Min(_total, (_nDownloadedTotal + bytesReceived));

                progressBarTotal.Value = Convert.ToInt32(100 * currentBytes / _total);
                labelTotal.Text = string.Format("{0} / {1}", (_nDownloadedTotal + bytesReceived), _total);
            }
        }

        #endregion

    }
}
