/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   ʥ����ʿ��Knights Warrior�� 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using AutoUpdater.Models;
using System.Linq;

namespace AutoUpdater.Forms
{
    public partial class DownloadConfirm : Form
    {
        #region The private fields
        readonly List<DownloadFileInfo> _downloadFileList;
        #endregion

        #region The constructor of DownloadConfirm
        public DownloadConfirm(List<DownloadFileInfo> downloadfileList)
        {
            InitializeComponent();

            _downloadFileList = downloadfileList;
        }
        #endregion

        #region The private method
        private void OnLoad(object sender, EventArgs e)
        {
            lb_lastVer.Text = _downloadFileList.Max(x => x.LastVer);
            lb_fileSizeMb.Text = string.Format("{0:#.##} Mb.", _downloadFileList.Sum(x => x.Size / 1024f / 1024f));
        }
        #endregion
    }
}