﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

namespace AutoUpdater.Configs
{
    public class ConstFile
    {
        /// <summary>
        /// 配置文件
        /// </summary>
        public const string FILENAME = "Update.config";

        /// <summary>
        /// 下载临时目录名（本程序目录下）
        /// </summary>
        public const string TEMPFOLDERNAME = "TempFolder";

        public const string CONFIGFILEKEY = "config_";
        public const string ROOLBACKFILE = "KnightsWarrior.exe";
        public const string CANCELORNOT = "KnightsWarrior Update is in progress. Do you really want to cancel?";

        /// <summary>
        /// 下载升级配置文件错误时的提示信息
        /// </summary>
        public const string NOTNETWORK = "KnightsWarrior.exe update is unsuccessful. KnightsWarrior.exe will now restart. Please try to update again.";

        /// <summary>
        /// 下载升级配置文件错误时的提示信息标题
        /// </summary>
        public const string MESSAGETITLE = "AutoUpdate Program";
    }
}
