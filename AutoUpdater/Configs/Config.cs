/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using AutoUpdater.Models;

namespace AutoUpdater.Configs
{
    /// <summary>
    /// 自动升级--配置信息
    /// </summary>
    [Serializable]
    [XmlRoot("configuration")]
    public class Config
    {
        #region The public property
        /// <summary>
        /// 启用自动升级功能
        /// </summary>
        public bool Enabled { set; get; }

        /// <summary>
        /// 主程序文件名
        /// </summary>
        public string MainProcess { get; set; }

        /// <summary>
        /// 远程升级文件地址
        /// </summary>
        public string ServerUrl { set; get; }

        /// <summary>
        /// 本地文件列表
        /// </summary>
        public List<LocalFile> UpdateFileList { set; get; }        // UpdateFileList
        #endregion

        #region 构造函数
        private static Config _instance;
        protected Config() { }

        #endregion

        #region 静态方法
        static Config()
        {
            try
            {
                _instance = Config.LoadConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.FILENAME));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static Config GetInstance()
        {
            return _instance;
        }

        protected static Config LoadConfig(string file)
        {
            var xs = new XmlSerializer(typeof(Config));
            using (var sr = new StreamReader(file))
            {
                var config = xs.Deserialize(sr) as Config;

                return config;
            }

        }
        #endregion

        #region The public method

        /// <summary>
        /// 将新文件、新版本的旧文件增加到Config中
        /// </summary>
        /// <param name="downloadList"></param>
        public void AppendUpdateFileList(List<DownloadFileInfo> downloadList)
        {
            var downloadFileNames = downloadList.Select(x => x.Path).ToList();
            //旧文件以外的文件
            var tempList = (from localFile in UpdateFileList
                            where false == downloadFileNames.Contains(localFile.Path)
                            select localFile
                            ).ToList();

            //增加下载列表中的文件
            var needAppend = downloadList.Select(x => new LocalFile()
            {
                Size = x.Size,
                LastVer = x.LastVer,
                Path = x.Path
            }).ToList();

            tempList.AddRange(needAppend);

            this.UpdateFileList = tempList.ToList();
        }

        public void SaveConfig(string file)
        {
            var xs = new XmlSerializer(typeof(Config));
            using (var sw = new StreamWriter(file))
            {
                xs.Serialize(sw, this);
            }
        }
        #endregion
    }

}
