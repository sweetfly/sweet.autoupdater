﻿using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;

namespace AutoUpdater.Utilities
{
    /// <summary>
    /// 操作进程
    /// </summary>
    public static class ProcessUtility
    {
        /// <summary>
        /// 启动进程
        /// </summary>
        /// <param name="fileName"></param>
        public static void StartProcess(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) { return; }

            var proc = new Process();
            proc.StartInfo.UseShellExecute = true;//是否使用操作系统外壳程序启动进程

            proc.StartInfo.WorkingDirectory = Path.GetDirectoryName(fileName);//启动进程的初始目录
            proc.StartInfo.FileName = fileName;
            proc.Start();
        }

        /// <summary>
        /// 杀死进程
        /// </summary>
        /// <param name="processName">进程名</param>
        public static int KillProcess(string processName)
        {
            if (string.IsNullOrEmpty(processName)) { return 0; }

            string processNameWater = Path.GetFileNameWithoutExtension(processName);

            Process[] arrPro = Process.GetProcessesByName(processNameWater);
            if (arrPro != null)
            {
                foreach (Process pro in arrPro) { pro.Kill(); }
            }
            return arrPro.Length;
        }

        /// <summary>
        /// 检查是否存在进程
        /// </summary>
        /// <param name="processName">进程名</param>
        /// <returns></returns>
        public static bool ExistProcess(string processName)
        {
            if (string.IsNullOrEmpty(processName)) { return false; }
            string procName = Path.GetFileNameWithoutExtension(processName);

            Process[] pro = Process.GetProcessesByName(procName);
            return pro.Any();
        }

    }
}
