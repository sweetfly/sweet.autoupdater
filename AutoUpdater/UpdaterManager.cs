﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AutoUpdater
{
    public class UpdaterManager
    {
        /// <summary>
        /// 运行自动升级程序进程
        /// </summary>
        public static void Run()
        {
            string fileName = Assembly.GetExecutingAssembly().Location;
            Process.Start(fileName);
        }
    }
}
