﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using AutoUpdater.Configs;
using AutoUpdater.Contracts;
using AutoUpdater.Forms;
using AutoUpdater.Models;
using AutoUpdater.Properties;
using AutoUpdater.Utilities;
using System.Linq;

namespace AutoUpdater.Implementations
{
    public class SweetAutoUpdater : IAutoUpdater
    {
        #region 私有成员
        private readonly Config _config = Config.GetInstance();

        /// <summary>
        /// 需要下载的文件列表
        /// </summary>
        List<DownloadFileInfo> _downloadFileListTemp = null;
        #endregion

        public bool Update()
        {
            if (!_config.Enabled) { return false; }

            //读取远程升级文件
            var listRemotFile = RemoteFile.ParseRemoteXml(_config.ServerUrl);
            //检查要下载的文件列表
            _downloadFileListTemp = DownloadFileInfo.InitDownloadFileList(_config.UpdateFileList, listRemotFile, new string[] { ConstFile.FILENAME });
            //需要重启主程序
            bool bNeedCloseMainProc = _downloadFileListTemp.Any(x => x.NeedClose);

            //不需要下载的情况
            if (_downloadFileListTemp == null || _downloadFileListTemp.Count <= 0)
            {
                return false;
            }

            //提示确认升级
            var dcForm = new DownloadConfirm(_downloadFileListTemp);
            if (DialogResult.OK != dcForm.ShowDialog())
            {
                //用户不同意升级
                return false;
            }

            //需要关闭主程序
            if (bNeedCloseMainProc) { ProcessUtility.KillProcess(_config.MainProcess); }

            //开始下载
            if (false == StartDownload(_downloadFileListTemp))
            {
                //下载失败
                return false;
            }

            //修改Config文件
            _config.AppendUpdateFileList(_downloadFileListTemp);

            //更新成功完成
            _config.SaveConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.FILENAME));

            //清理工作
            ClearUp();

            MessageBox.Show(Resource.Message_Update_Success_Content, Resource.Message_Update_Success_Title, MessageBoxButtons.OK, MessageBoxIcon.Information);

            //使主程序运行
            if (false == ProcessUtility.ExistProcess(_config.MainProcess)) { ProcessUtility.StartProcess(_config.MainProcess); }

            //更新成功
            return true;
        }


        public bool RollBack()
        {
            return false;
        }



        #region 私有方法

        private bool StartDownload(List<DownloadFileInfo> downloadList)
        {
            var dlg = new SweetDownloadProgress(downloadList);
            var result = dlg.ShowDialog() == DialogResult.OK;

            return result;
        }

        /// <summary>
        /// 更新完成后的清理工作
        /// </summary>
        private static void ClearUp()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.TEMPFOLDERNAME);

            //Delete the temp folder
            //if (Directory.Exists(path))
            //{
            //    Directory.Delete(path, true);
            //}
        }
        #endregion
    }
}
