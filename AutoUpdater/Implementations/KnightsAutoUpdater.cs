/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using AutoUpdater.Configs;
using AutoUpdater.Contracts;
using AutoUpdater.Forms;
using AutoUpdater.Models;
using AutoUpdater.Properties;
using AutoUpdater.Utilities;
using System.Linq;

namespace AutoUpdater.Implementations
{
    public class KnightsAutoUpdater : IAutoUpdater
    {
        #region The private fields
        private readonly Config _config = Config.GetInstance();

        /// <summary>
        /// 升级时需要关闭主程序
        /// </summary>
        private bool _bNeedCloseMainProc = false;

        /// <summary>
        /// 需要下载的文件列表
        /// </summary>
        List<DownloadFileInfo> _downloadFileListTemp = null;
        #endregion

        #region The public event
        public event Action OnShow;
        #endregion

        #region The public method

        /// <summary>
        /// 更新
        /// </summary>
        public bool Update()
        {
            if (!_config.Enabled) { return false; }

            //读取远程升级文件
            Dictionary<string, RemoteFile> listRemotFile = RemoteFile.ParseRemoteXml(_config.ServerUrl).ToDictionary(x => x.Path, x => x);

            //检查要下载的文件列表
            _downloadFileListTemp = InitDownloadFileList(listRemotFile);

            //不需要下载的情况
            if (_downloadFileListTemp == null || _downloadFileListTemp.Count <= 0)
            {
                return false;
            }

            //触发OnShow事件
            if (this.OnShow != null) { this.OnShow(); }

            //提示确认升级
            var dcForm = new DownloadConfirm(_downloadFileListTemp);
            if (DialogResult.OK != dcForm.ShowDialog())
            {
                //用户不同意升级
                return false;
            }

            //需要关闭主程序
            if (_bNeedCloseMainProc) { ProcessUtility.KillProcess(_config.MainProcess); }

            //开始下载
            if (false == StartDownload(_downloadFileListTemp))
            {
                //下载失败
                return false;
            }

            //更新成功完成
            _config.SaveConfig(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.FILENAME));

            //清理工作
            ClearUp();

            MessageBox.Show(Resource.Message_Update_Success_Content, Resource.Message_Update_Success_Title, MessageBoxButtons.OK, MessageBoxIcon.Information);

            //使主程序运行
            if (false == ProcessUtility.ExistProcess(_config.MainProcess)) { ProcessUtility.StartProcess(_config.MainProcess); }

            //更新成功
            return true;

        }

        /// <summary>
        /// 回滚
        /// </summary>
        public bool RollBack()
        {
            foreach (DownloadFileInfo file in _downloadFileListTemp)
            {
                string tempUrlPath = CommonUnitity.GetFolderUrl(file);
                string oldPath = string.Empty;
                try
                {
                    if (!string.IsNullOrEmpty(tempUrlPath))
                    {
                        oldPath = Path.Combine(CommonUnitity.SystemBinUrl + tempUrlPath.Substring(1), file.FileName);
                    }
                    else
                    {
                        oldPath = Path.Combine(CommonUnitity.SystemBinUrl, file.FileName);
                    }

                    if (oldPath.EndsWith("_"))
                        oldPath = oldPath.Substring(0, oldPath.Length - 1);

                    MoveFolderToOld(oldPath + ".old", oldPath);

                }
                catch (Exception ex)
                {
                    //log the error message,you can use the application's log code
                }
            }
            return true;
        }

        #endregion

        #region The private method
        private void MoveFolderToOld(string oldPath, string newPath)
        {
            if (File.Exists(oldPath) && File.Exists(newPath))
            {
                System.IO.File.Copy(oldPath, newPath, true);
            }
        }

        /// <summary>
        /// 生成需要更新的文件
        /// </summary>
        /// <param name="listRemotFile"></param>
        /// <returns></returns>
        private List<DownloadFileInfo> InitDownloadFileList(Dictionary<string, RemoteFile> listRemotFile)
        {
            var downloadList = new List<DownloadFileInfo>();

            //遍历本地文件列表(配置文件读取)
            foreach (LocalFile file in _config.UpdateFileList)
            {
                if (listRemotFile.ContainsKey(file.Path))
                {
                    RemoteFile remoteFile = listRemotFile[file.Path];
                    Version remoteVer = new Version(remoteFile.LastVer);
                    Version localVer = new Version(file.LastVer);

                    //远程升级文件中存在此文件信息,比较版本号
                    if (remoteVer > localVer)
                    {
                        //添加到下载列表
                        downloadList.Add(new DownloadFileInfo()
                            {
                                DownloadUrl = remoteFile.Url,
                                LastVer = remoteFile.LastVer,
                                Size = remoteFile.Size,
                                Path = file.Path
                            });

                        //修改本地版本号
                        file.LastVer = remoteFile.LastVer;
                        file.Size = remoteFile.Size;

                        if (remoteFile.NeedClose)
                            _bNeedCloseMainProc = true;
                    }

                    listRemotFile.Remove(file.Path);
                }
            }

            //将新增文件加入下载列表
            foreach (RemoteFile file in listRemotFile.Values)
            {
                downloadList.Add(new DownloadFileInfo()
                    {
                        DownloadUrl = file.Url,
                        Path = file.Path,
                        LastVer = file.LastVer,
                        Size = file.Size
                    });

                if (file.NeedClose)
                    _bNeedCloseMainProc = true;
            }

            return downloadList;
        }

        private bool StartDownload(List<DownloadFileInfo> downloadList)
        {
            var dlg = new SweetDownloadProgress(downloadList);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                ////用户取消升级
                //if (DialogResult.Cancel == dlg.ShowDialog())
                //{
                //    return false;
                //}
                return true;
            }

            return false;
        }

        /// <summary>
        /// 更新完成后的清理工作
        /// </summary>
        private static void ClearUp()
        {
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConstFile.TEMPFOLDERNAME);
            //Delete the temp folder
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }

        #endregion

    }

}
