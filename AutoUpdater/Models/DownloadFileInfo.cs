﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AutoUpdater.Models
{
    [Serializable]
    public class DownloadFileInfo
    {

        #region The public property

        /// <summary>
        /// 远程地址
        /// </summary>
        public string DownloadUrl { get; set; }

        /// <summary>
        /// 本地文件路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 得到文件名 + 扩展名
        /// </summary>
        public string FileName
        {
            get { return System.IO.Path.GetFileName(Path); }
        }

        /// <summary>
        /// 最新版本号
        /// </summary>
        public string LastVer { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 升级时，需要关闭主程序
        /// </summary>
        public bool NeedClose { get; set; }
        #endregion

        /// <summary>
        /// 生成需要更新的文件
        /// </summary>
        /// <param name="listLocalFile"></param>
        /// <param name="listRemotFile"></param>
        /// <returns></returns>
        public static List<DownloadFileInfo> InitDownloadFileList(List<LocalFile> listLocalFile, List<RemoteFile> listRemotFile, IEnumerable<string> ignoreFilePath = null)
        {
            var localFileNames = listLocalFile.Select(x => x.Path).ToList();

            //新增加的文件
            var newFiles = (from remoteFile in listRemotFile
                            where false == localFileNames.Contains(remoteFile.Path)
                            select remoteFile
                           ).ToList();

            //有新版本的文件
            var newVerFiles = (from localFile in listLocalFile
                               join remoteFile in listRemotFile on localFile.Path equals remoteFile.Path
                               where new Version(remoteFile.LastVer) > new Version(localFile.LastVer)
                               select remoteFile
                              ).ToList();

            //所有需要下载的文件
            var tempList = newFiles.Concat(newVerFiles);

            if (ignoreFilePath != null)
            {
                tempList = tempList.Where(x => false == ignoreFilePath.Contains(x.Path));
            }

            var result = tempList.Select(x => new DownloadFileInfo()
             {
                 DownloadUrl = x.Url,
                 LastVer = x.LastVer,
                 Size = x.Size,
                 Path = x.Path,
                 NeedClose = x.NeedClose
             }).ToList();

            return result;
        }

    }
}
