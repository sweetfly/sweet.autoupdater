﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

using System;
using System.Xml.Serialization;

namespace AutoUpdater.Models
{
    /// <summary>
    /// 本地文件信息
    /// </summary>
    [Serializable]
    public class LocalFile
    {

        #region The public property
        /// <summary>
        /// 文件路径
        /// </summary>
        [XmlAttribute("Path")]
        public string Path { get; set; }

        /// <summary>
        /// 最后版本号
        /// </summary>
        [XmlAttribute("LastVer")]
        public string LastVer { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        [XmlAttribute("Size")]
        public int Size { get; set; }
        #endregion

        #region The constructor of LocalFile
        public LocalFile(string path, string ver, int size)
        {
            this.Path = path;
            this.LastVer = ver;
            this.Size = size;
        }

        public LocalFile()
        {
        }
        #endregion

    }
}
