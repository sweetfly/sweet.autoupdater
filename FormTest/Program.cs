﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using AutoUpdater.Utilities;
using System.Diagnostics;
using System.IO;

namespace FormTest
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //自动升级
            const string updaterName = "AutoUpdater.exe";
            if (File.Exists(updaterName)) { Process.Start(updaterName); }

            Application.Run(new MainForm());

        }
    }
}
