﻿namespace FormTest
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label2;
            this.button1 = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lb_status = new System.Windows.Forms.Label();
            this.btn_download = new System.Windows.Forms.Button();
            this.txt_uri = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(80, 396);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "我是程序";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(16, 58);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(563, 23);
            this.progressBar1.TabIndex = 1;
            // 
            // lb_status
            // 
            this.lb_status.AutoSize = true;
            this.lb_status.Location = new System.Drawing.Point(592, 65);
            this.lb_status.Name = "lb_status";
            this.lb_status.Size = new System.Drawing.Size(41, 12);
            this.lb_status.TabIndex = 2;
            this.lb_status.Text = "label1";
            // 
            // btn_download
            // 
            this.btn_download.Location = new System.Drawing.Point(594, 29);
            this.btn_download.Name = "btn_download";
            this.btn_download.Size = new System.Drawing.Size(75, 23);
            this.btn_download.TabIndex = 3;
            this.btn_download.Text = "Download";
            this.btn_download.UseVisualStyleBackColor = true;
            // 
            // txt_uri
            // 
            this.txt_uri.Location = new System.Drawing.Point(55, 31);
            this.txt_uri.Name = "txt_uri";
            this.txt_uri.Size = new System.Drawing.Size(524, 21);
            this.txt_uri.TabIndex = 4;
            this.txt_uri.Text = "http://softdownload.hao123.com/hao123-soft-online-bcs/soft/C/Codematic_2.78.zip";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(14, 36);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(35, 12);
            label2.TabIndex = 2;
            label2.Text = "Url：";
            // 
            // MainForm
            // 
            this.AcceptButton = this.btn_download;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(681, 431);
            this.Controls.Add(this.txt_uri);
            this.Controls.Add(this.btn_download);
            this.Controls.Add(label2);
            this.Controls.Add(this.lb_status);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.button1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lb_status;
        private System.Windows.Forms.Button btn_download;
        private System.Windows.Forms.TextBox txt_uri;
    }
}

