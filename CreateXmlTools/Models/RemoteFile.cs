﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace AutoUpdater.Models
{
    /// <summary>
    /// 远程升级配置中的文件信息
    /// </summary>
    [Serializable]
    public class RemoteFile
    {
        #region The public property
        /// <summary>
        /// 本地路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 远程下载地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 文件大小
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// 最新版本号
        /// </summary>
        public string LastVer { get; set; }

        /// <summary>
        /// 升级时，需要关闭主程序
        /// </summary>
        public bool NeedClose { get; set; }
        #endregion

        #region The constructor of AutoUpdater
        public RemoteFile()
        {

        }
        public RemoteFile(XmlNode node)
        {
            if (node == null || node.Attributes == null)
            {
                throw new ArgumentNullException("node");
            }

            this.Path = node.Attributes["Path"].Value;
            this.Url = node.Attributes["Url"].Value;
            this.LastVer = node.Attributes["LastVer"].Value;
            this.Size = Convert.ToInt32(node.Attributes["Size"].Value);
            this.NeedClose = Convert.ToBoolean(node.Attributes["NeedClose"].Value);
        }
        #endregion

        #region 静态方法

        /// <summary>
        /// 读取Xml文件，返回一个字典
        /// </summary>
        /// <param name="xml">数据源文件</param>
        /// <returns></returns>
        public static Dictionary<string, RemoteFile> ParseRemoteXml(string xml)
        {
            XmlDocument document = new XmlDocument();
            document.Load(xml);

            var result = document.DocumentElement.ChildNodes.Cast<XmlNode>()
                            .ToDictionary(node => node.Attributes != null ? node.Attributes["Path"].Value : null,
                                          node => new RemoteFile(node));

            return result;
        }

        #endregion
    }
}
