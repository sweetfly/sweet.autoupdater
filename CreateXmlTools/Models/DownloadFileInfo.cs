﻿/*****************************************************************
 * Copyright (C) Knights Warrior Corporation. All rights reserved.
 * 
 * Author:   圣殿骑士（Knights Warrior） 
 * Email:    KnightsWarrior@msn.com
 * Website:  http://www.cnblogs.com/KnightsWarrior/       http://knightswarrior.blog.51cto.com/
 * Create Date:  5/8/2010 
 * Usage:
 *
 * RevisionHistory
 * Date         Author               Description
 * 
*****************************************************************/

using System;
using System.IO;

namespace AutoUpdater.Models
{
    [Serializable]
    public class DownloadFileInfo
    {
        #region The private fields
        readonly string _fileName = string.Empty;

        #endregion

        #region The public property

        /// <summary>
        /// 远程地址
        /// </summary>
        public string DownloadUrl
        {
            get;
            protected set;
        }

        /// <summary>
        /// 本地文件路径
        /// </summary>
        public string FileFullName
        {
            get { return _fileName; }
        }

        /// <summary>
        /// 得到文件名 + 扩展名
        /// </summary>
        public string FileName
        {
            get { return Path.GetFileName(FileFullName); }
        }

        /// <summary>
        /// 最新版本号
        /// </summary>
        public string LastVer
        {
            get;
            protected set;
        }

        /// <summary>
        /// 文件大小
        /// </summary>
        public int Size
        {
            get;
            protected set;
        }
        #endregion

        #region The constructor of DownloadFileInfo
        public DownloadFileInfo(string url, string name, string ver, int size)
        {
            this.DownloadUrl = url;
            this._fileName = name;
            this.LastVer = ver;
            this.Size = size;
        }
        #endregion
    }
}
