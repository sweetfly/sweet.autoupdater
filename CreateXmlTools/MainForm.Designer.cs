﻿namespace CreateXmlTools
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.SplitContainer splitContainer1;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label1;
            this.txt_main_verion = new System.Windows.Forms.TextBox();
            this.txt_main_baseUrl = new System.Windows.Forms.TextBox();
            this.btn_main_selectDir = new System.Windows.Forms.Button();
            this.btn_main_generate = new System.Windows.Forms.Button();
            this.txt_main_procBaseDir = new System.Windows.Forms.TextBox();
            this.txt_main_result = new System.Windows.Forms.TextBox();
            this.cb_closeMainProc = new System.Windows.Forms.CheckBox();
            splitContainer1 = new System.Windows.Forms.SplitContainer();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            splitContainer1.Location = new System.Drawing.Point(0, 0);
            splitContainer1.Name = "splitContainer1";
            splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(this.cb_closeMainProc);
            splitContainer1.Panel1.Controls.Add(this.txt_main_verion);
            splitContainer1.Panel1.Controls.Add(label3);
            splitContainer1.Panel1.Controls.Add(this.txt_main_baseUrl);
            splitContainer1.Panel1.Controls.Add(label2);
            splitContainer1.Panel1.Controls.Add(this.btn_main_selectDir);
            splitContainer1.Panel1.Controls.Add(this.btn_main_generate);
            splitContainer1.Panel1.Controls.Add(label1);
            splitContainer1.Panel1.Controls.Add(this.txt_main_procBaseDir);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(this.txt_main_result);
            splitContainer1.Size = new System.Drawing.Size(658, 415);
            splitContainer1.SplitterDistance = 81;
            splitContainer1.TabIndex = 4;
            // 
            // txt_main_verion
            // 
            this.txt_main_verion.Location = new System.Drawing.Point(556, 45);
            this.txt_main_verion.Name = "txt_main_verion";
            this.txt_main_verion.Size = new System.Drawing.Size(90, 21);
            this.txt_main_verion.TabIndex = 13;
            this.txt_main_verion.Text = "1.0.0.0";
            this.txt_main_verion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(492, 48);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(53, 12);
            label3.TabIndex = 11;
            label3.Text = "版本号：";
            // 
            // txt_main_baseUrl
            // 
            this.txt_main_baseUrl.Location = new System.Drawing.Point(92, 45);
            this.txt_main_baseUrl.Name = "txt_main_baseUrl";
            this.txt_main_baseUrl.Size = new System.Drawing.Size(278, 21);
            this.txt_main_baseUrl.TabIndex = 14;
            this.txt_main_baseUrl.Text = "http://www.SweetFly.net";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(23, 48);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(35, 12);
            label2.TabIndex = 12;
            label2.Text = "Url：";
            // 
            // btn_main_selectDir
            // 
            this.btn_main_selectDir.Location = new System.Drawing.Point(490, 10);
            this.btn_main_selectDir.Name = "btn_main_selectDir";
            this.btn_main_selectDir.Size = new System.Drawing.Size(75, 23);
            this.btn_main_selectDir.TabIndex = 10;
            this.btn_main_selectDir.Text = "选择目录";
            this.btn_main_selectDir.UseVisualStyleBackColor = true;
            this.btn_main_selectDir.Click += new System.EventHandler(this.btn_main_selectDir_Click);
            // 
            // btn_main_generate
            // 
            this.btn_main_generate.Location = new System.Drawing.Point(571, 10);
            this.btn_main_generate.Name = "btn_main_generate";
            this.btn_main_generate.Size = new System.Drawing.Size(75, 23);
            this.btn_main_generate.TabIndex = 9;
            this.btn_main_generate.Text = "生成文件";
            this.btn_main_generate.UseVisualStyleBackColor = true;
            this.btn_main_generate.Click += new System.EventHandler(this.btn_main_generate_Click);
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(21, 15);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(65, 12);
            label1.TabIndex = 8;
            label1.Text = "程序目录：";
            // 
            // txt_main_procBaseDir
            // 
            this.txt_main_procBaseDir.Location = new System.Drawing.Point(92, 12);
            this.txt_main_procBaseDir.Name = "txt_main_procBaseDir";
            this.txt_main_procBaseDir.Size = new System.Drawing.Size(392, 21);
            this.txt_main_procBaseDir.TabIndex = 7;
            // 
            // txt_main_result
            // 
            this.txt_main_result.AcceptsReturn = true;
            this.txt_main_result.AcceptsTab = true;
            this.txt_main_result.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_main_result.Location = new System.Drawing.Point(0, 0);
            this.txt_main_result.Multiline = true;
            this.txt_main_result.Name = "txt_main_result";
            this.txt_main_result.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_main_result.Size = new System.Drawing.Size(658, 330);
            this.txt_main_result.TabIndex = 4;
            // 
            // cb_closeMainProc
            // 
            this.cb_closeMainProc.AutoSize = true;
            this.cb_closeMainProc.Checked = true;
            this.cb_closeMainProc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_closeMainProc.Location = new System.Drawing.Point(376, 47);
            this.cb_closeMainProc.Name = "cb_closeMainProc";
            this.cb_closeMainProc.Size = new System.Drawing.Size(108, 16);
            this.cb_closeMainProc.TabIndex = 15;
            this.cb_closeMainProc.Text = "需要关闭主程序";
            this.cb_closeMainProc.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AcceptButton = this.btn_main_generate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 415);
            this.Controls.Add(splitContainer1);
            this.Name = "MainForm";
            this.Text = "升级文件生成工具";
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel1.PerformLayout();
            splitContainer1.Panel2.ResumeLayout(false);
            splitContainer1.Panel2.PerformLayout();
            splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txt_main_verion;
        private System.Windows.Forms.TextBox txt_main_baseUrl;
        private System.Windows.Forms.Button btn_main_selectDir;
        private System.Windows.Forms.Button btn_main_generate;
        private System.Windows.Forms.TextBox txt_main_procBaseDir;
        private System.Windows.Forms.TextBox txt_main_result;
        private System.Windows.Forms.CheckBox cb_closeMainProc;

    }
}

