﻿using AutoUpdater.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using CreateXmlTools.Properties;

namespace CreateXmlTools
{
    public partial class MainForm : Form
    {
        private readonly string _saveFileName = new Settings().XmlSaveFileName;

        public MainForm()
        {
            InitializeComponent();
            init();
        }
        void init()
        {
            txt_main_procBaseDir.Text = Environment.CurrentDirectory;
        }

        #region 按钮事件
        private void btn_main_selectDir_Click(object sender, EventArgs e)
        {
            var dlg = new FolderBrowserDialog();
            if (DialogResult.OK == dlg.ShowDialog())
            {
                txt_main_procBaseDir.Text = dlg.SelectedPath;
            }
        }

        private void btn_main_generate_Click(object sender, EventArgs e)
        {
            string path = txt_main_procBaseDir.Text.Trim();
            string urlBase = txt_main_baseUrl.Text.Trim();
            Version version = new Version(txt_main_verion.Text.Trim());
            bool needClose = cb_closeMainProc.Checked;

            if (string.IsNullOrEmpty(path))
            {
                MessageBox.Show(Resources.MainForm_btn_main_generate_Click_SelectFolder);
                return;
            }
            var fileList = ReadFileInfo(path, urlBase, version, needClose);

            XDocument doc = GenerateXml(fileList);

            doc.Save(_saveFileName);
            MessageBox.Show(Resources.Message_Save_Success_Content + _saveFileName);

            txt_main_result.Text = doc.ToString();
        }


        #endregion

        #region 私有方法

        /// <summary>
        /// 遍历目录，读取文件信息
        /// </summary>
        /// <param name="targetPath"></param>
        /// <returns></returns>
        private static List<RemoteFile> ReadFileInfo(string targetPath, string urlBase, Version version, bool needClose)
        {
            var list = new List<RemoteFile>();

            DirectoryInfo dicInfo = new DirectoryInfo(targetPath);
            var allFiles = dicInfo.GetFiles("*.*", SearchOption.AllDirectories);

            foreach (var file in allFiles)
            {
                var relativePath = file.FullName.Replace(targetPath + @"\", string.Empty);
                var model = new RemoteFile()
                {
                    Path = relativePath,
                    Size = (int)file.Length,
                    Url = Path.Combine(urlBase, relativePath).Replace(@"\", "/"),
                    LastVer = version.ToString(),
                    NeedClose = needClose
                };
                list.Add(model);
            }
            return list;
        }

        private static XDocument GenerateXml(List<RemoteFile> fileList)
        {
            var props = typeof(RemoteFile).GetProperties(BindingFlags.Instance | BindingFlags.Public);

            XDocument doc = new XDocument();
            var root = new XElement("root");
            doc.Add(root);

            foreach (var file in fileList)
            {
                var fileItem = new XElement("fileItem");
                root.Add(fileItem);

                //遍历所有属性
                foreach (var item in props)
                {
                    object value = item.GetValue(file, null);
                    fileItem.Add(new XAttribute(item.Name, value == null ? string.Empty : value.ToString()));
                }
            }
            return doc;
        }

        #endregion
    }
}
